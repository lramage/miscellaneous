#!/bin/bash
# Setup New Git Repository on Multiple Remotes

# Github
# Your API Token can be found on the GitHub site, click Account Settings, 
# look for Administrative Information and API Token (32 character long string).
curl -u 'USER' https://api.github.com/user/repos -d '{"name":"REPO"}'
git remote add origin git@github.com:USER/REPO.git

# Gitlab
curl -H "Content-Type:application/json" https://gitlab.com/api/v3/projects?private_token=$token -d "{ \"name\": \"$repo\" }"

# Bitbucket
curl --user login:pass https://api.bitbucket.org/1.0/repositories/ \
--data name=REPO_NAME # --data is_private='true' # for private repositories