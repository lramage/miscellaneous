# Gentoo Catalyst Automation Buildbot 

- [Catalyst](https://wiki.gentoo.org/wiki/Catalyst)

## Installing

dependencies:
- python3
- sqlite3
- pip
- virtualenv

```
  $ virtualenv catalyst

  $ cd catalyst && . bin/activate

  $ pip install buildbot[bundle]

  $ mkdir -p var/tmp/{catalyst/{builds,snapshots},buildbot}

  $ cp master.cfg var/tmp/buildbot/ && cd var/tmp/buildbot

  $ buildbot upgrade-master
```
