# Hex OS
Container driven, gentoo-based desktop linux for personal use.

## Packages
- [app-editors/neovim](https://packages.gentoo.org/packages/app-editors/neovim)
- [app-emulation/lxc](https://wiki.gentoo.org/wiki/LXC)
- [dev-vcs/git](https://wiki.gentoo.org/wiki/Git)
- [media-fonts/terminus-font](https://packages.gentoo.org/packages/media-fonts/terminus-font)
- [media-sound/alsa-utils](https://wiki.gentoo.org/wiki/ALSA#Installation)
- [net-firewall/iptables](https://wiki.gentoo.org/wiki/Iptables)
- [net-misc/keychain](https://wiki.gentoo.org/wiki/Keychain)
- [sys-apps/lm_sensors](https://wiki.gentoo.org/wiki/Lm_sensors)
- [x11-misc/compton](https://packages.gentoo.org/packages/x11-misc/compton)
- [x11-misc/slock](https://packages.gentoo.org/packages/x11-misc/slock)
- [x11-terms/st](https://packages.gentoo.org/packages/x11-terms/st) + [scrolling patch](http://st.suckless.org/patches/scrollback)
- [x11-apps/transset](https://packages.gentoo.org/packages/x11-apps/transset)
- [x11-wm/xwm](https://github.com/lramage94/xwm)
- [www-client/chromium](https://packages.gentoo.org/packages/www-client/chromium)

## Theming
- [Base16](https://github.com/chriskempson/base16)

## Resources
- [Alpine Linux](https://alpinelinux.org/)
- [Barebox](http://www.barebox.org/)
- [Buildroot](https://buildroot.uclibc.org/)
- [Calculate Linux](http://www.calculate-linux.org/)
- [Catalyst](https://wiki.gentoo.org/wiki/Catalyst)
- [Cheat Sheet](https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet)
- [Core OS Overlay](https://github.com/coreos/coreos-overlay)
- [Custom Profile](https://wiki.gentoo.org/wiki/Profile_(Portage))
- [Custom Repository](https://wiki.gentoo.org/wiki/Handbook:AMD64/Portage/CustomTree#Defining_a_custom_repository)
- [Easy Gentoo](https://github.com/lramage94/easygentoo)
- [Flying With Gentoo](https://forums.gentoo.org/viewtopic.php?t=231170)
- [Gentoo Linux](https://www.gentoo.org/)
- [Gentoo Linux Base Profile Packages](https://gitweb.gentoo.org/repo/gentoo.git/tree/profiles/base/packages)
- [Gentoo Cloud Kernel Config](https://gitweb.gentoo.org/proj/releng.git/tree/releases/weekly/kconfig/amd64/cloud-amd64-gentoo.config)
- [Gentoo Linux Installer (legacy)](https://gitweb.gentoo.org/proj/gli.git/)
- [Gentoo Linux LiveUSB](https://wiki.gentoo.org/wiki/LiveUSB/Guide)
- [Gentoo Linux Signed Kernel Module Support](https://wiki.gentoo.org/wiki/Signed_kernel_module_support)
- [Gentoo Install Checklist (hades)](http://hades.github.io/2011/01/gentoo-install-checklist/)
- [Gentoo Profiles](https://gitweb.gentoo.org/repo/gentoo.git/tree/profiles)
- [Gentoo Quick Installation Checklist](https://wiki.gentoo.org/wiki/Quick_Installation_Checklist)
- [Gentoo Release Tools](https://gitweb.gentoo.org/proj/releng.git/tree/)
- [Kbuild Skeleton](https://github.com/lramage94/kbuild_skeleton)
- [Linux](https://github.com/torvalds/linux)
- [LEDE](https://github.com/lramage94/LEDE)
- [LiveCD for Dummies](https://forums.gentoo.org/viewtopic-t-244837.html)
- [Simple Init](http://ewontfix.com/14/)
- [Stage Tarball](https://wiki.gentoo.org/wiki/Stage_tarball)
- [Quickstart](https://github.com/lramage94/quickstart)
- [Ultimate Linux Desktop](https://blog.jessfraz.com/post/ultimate-linux-on-the-desktop/)

See also,

https://wiki.gentoo.org/wiki/Profile_(Portage)

https://github.com/PagerDuty/Nut

https://libvirt.org/drvlxc.html

http://zeromq.org

http://www.armadeus.org/wiki/index.php?title=Linux_Boot_Logo
https://github.com/lramage94/linux/blob/master/drivers/video/logo/logo.c

vertical on right side

http://dev.chromium.org/chromium-os/chromiumos-design-docs/filesystem-autoupdate
https://github.com/ostreedev/ostree

https://github.com/ulfox/GSE
