#!/bin/bash
# Install needed packages for Steam

# You need to enable multilib repos to install steam
PACKAGES="sudo base base-devel bash-completion screen vim steam"

pacman -S $PACKAGES

# Add a steam user account
useradd -m -G wheel,video,audio -s /bin/bash steam

# Autostart Steam
echo "/usr/local/bin/steam-chroot-autorun.sh &" >> /home/steam/.bashrc