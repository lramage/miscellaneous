#!/bin/bash
# Setup steam chroot and launch steam

STEAM_DIR="/usr/local/steam"

# Share the $DISPLAY
xhost +local: &> /dev/null

sudo mount --rbind /proc $STEAM_DIR/proc
sudo mount --rbind /dev $STEAM_DIR/dev
sudo mount --rbind /sys $STEAM_DIR/sys
sudo mount --rbind /run $STEAM_DIR/run

sudo chroot $STEAM_DIR/ /bin/bash