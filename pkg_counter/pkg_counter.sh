#!/bin/bash
# this needs to be run from the aports root directory 
for repo in main community testing non-free unmaintained; do 
  # updated per <svyatoslav.mishyn@gmail.com> in the mailing list
  printf "%-15s%d\n" "$repo:" "$((find $repo -name APKBUILD) | wc -l)"
done