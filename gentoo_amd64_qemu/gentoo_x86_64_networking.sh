#!/bin/bash
# Setup disk image to install Gentoo via Qemu

# https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Minimal_installation_CD
# Minimum specifications
# CPU	Any AMD64 CPU or EM64T CPU (Core 2 Duo & Quad processors are EM64T)
# Memory	256 MB
# Disk space	2.5 GB (excluding swap space)
# Swap space	At least 256 MB
if [ ! -f gentoo.img ]; then
  qemu-img create -f raw gentoo.img 2.756G
else
  echo "Disk image already exists"
fi

if [ -f ./gentoo.img ]; then
  wget http://distfiles.gentoo.org/releases/amd64/autobuilds/20161006/install-amd64-minimal-20161006.iso
  
  if [ -f ./install-amd64-minimal-20161006.iso ]; then
    mv install-amd64-minimal-20161006.iso gentoo.iso
    
    if [ -f ./gentoo.iso ]; then
      qemu-system-x86_64 -m 256 -hda gentoo.img -cdrom gentoo.iso -boot d -netdev user,id=network0 -device e1000,netdev=network0 &
    else
      echo "Error: could not start qemu"
    fi
    
  else
    echo "Error: could not download iso"
  fi
  
else
  echo "Error: could not create disk image"
fi