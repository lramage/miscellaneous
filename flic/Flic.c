#include <stdio.h>
#include <string.h>

int main( int argc, char *argv[] ) {
        if( argc == 2 ) {

                char command[50];

                strcpy( command, "ping -c 1 -w 1 " );
                strcat( command, argv[1] );
                strcat( command, " > /dev/null" );
                printf(system(command));
        } else {
                printf("Usage: flick <ip addr>\n");
        }
        return(0);
}
