# Alpine Linux Custom Image for OpenStack
- [OpenStack Documentation: Custom Images](https://docs.openstack.org/image-guide/create-images-manually.html)
- [OpenStack Documentation: FreeBSD Image](https://docs.openstack.org/image-guide/freebsd-image.html)
- [Partitioning Scheme](https://wiki.alpinelinux.org/wiki/Setting_up_disks_manually)

See also,

[Setting up disks manually](https://wiki.alpinelinux.org/wiki/Setting_up_disks_manually "Alpine Wiki")

[Install to disk](https://wiki.alpinelinux.org/wiki/Install_to_disk)
