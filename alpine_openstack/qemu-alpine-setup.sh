#!/bin/sh
# Qemu Setup for Alpine

# Fetch alpine virtual image
wget https://nl.alpinelinux.org/alpine/v3.6/releases/x86_64/alpine-virt-3.6.0-x86_64.iso

# Create disk image
qemu-img create -f qcow2 /tmp/alpine.qcow2 1G

# Generate qemu launcher script
cat <<'EOF' >> alpine-livecd.sh
#!/bin/sh
# alpine live cd in qemu

qemu-system-x86_64 -m 512 \
                   -hda /tmp/alpine.qcow2 \
                   -cdrom alpine-virt-3.6.0-x86_64.iso \
                   -netdev user,id=network0 -device e1000,netdev=network0 \
                   -boot d &
EOF
chmod +x ./alpine-livecd.sh