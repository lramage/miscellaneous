#!/usr/bin/awk -f
# Extract svn commits from mantis bug tracker
# by: Lucas Ramage (lramage) 2017
# usage: mantis-svn2csv.awk mantis.txt > commits.csv

/Changeset/ { gsub(/\[|\]/, ""); print $2" "$4}
