# Subversion Commits in Mantis Bug Tracker to CSV.

- Navigate to Active Mantis Ticket in web browser.
- Select (Command + a) and Copy (Command + c) everything on the page.
- Open terminal.
<pre>

  $ cat > mantis.txt
  # Paste (Command + v), then Cancel (Command + c)
  
  $ mantis-svn2csv.awk mantis.txt > commits.csv
  
</pre>

See also,

    mantis_extract_svn.awk mantis_num | sort | awk '/repo/ { printf "%s,", $1 } END { print "" }'
