# Buildbot as Web-based Gentoo Installer

- [Buildbot](https://buildbot.net/)
- [Gentoo Wiki: AMD64 Installation Handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation)

See also,

- https://github.com/agaffney/quickstart
- https://github.com/r1k0/kicktoo
- https://github.com/gentoo/stager

https://wiki.gentoo.org/wiki/Quick_Installation_Checklist

```console
$ wget https://bootstrap.pypa.io/get-pip.py && \
python get-pip.py --user && \
~/.local/bin/pip install virtualenv --user && \
~/.local/bin/virtualenv /root/venv

$ . venv/bin/activate && \
pip install buildbot[bundle]
```

```console
IP_ADDR=$(ip addr show dev eth0 | awk 'FNR == 3 { split($2, a, "/"); print a[1] }')
BUILDBOT_URL="${IP_ADDR}:8010"
qrencode -t ASCII $BUILDBOT_URL &&
echo The web interface is located at $BUILDBOT_URL
```

```console
for sqlite support in python,
USE="$USE sqlite"
```
