[Gentoo Wiki: Qemu](https://wiki.gentoo.org/wiki/QEMU/Options)

[Notes on Qemu Source Code](http://chenyufei.info/notes/qemu-src.html)

![](https://i.stack.imgur.com/WI20w.jpg)

[Building a Shared Library](http://www.yolinux.com/TUTORIALS/LibraryArchives-StaticAndDynamic.html)

    $ gcc -shared -Wl,-soname,lib<name>.so.1 -o lib<name>.so.1.0 *.o

[Qemu Shared Folder](https://wiki.qemu.org/Documentation/9psetup)

    -virtfs fsdriver,id=[id],path=[path to share],security_model=[mapped|passthrough|none][,writeout=writeout][,readonly]
     [,socket=socket|sock_fd=sock_fd],mount_tag=[mount tag]

[Tiny Core Linux: Mount Mode](http://www.tinycorelinux.net/concepts.html)

    -append "tce=sda"

[GDB: Misc Commands](https://www.gnu.org/software/gdb/)

    (gdb) break <function name>

    (gdb) set/show args <args list>

    (gdb) run

    (gdb) n <next>