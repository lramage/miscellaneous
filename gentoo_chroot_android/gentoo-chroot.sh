#!/bin/bash
# Setup gentoo chroot

su - << EOF

mount -t proc proc /data/gentoo/proc
busybox mount --rbind /dev /data/gentoo/dev
busybox mount --rbind /sys /data/gentoo/sys

chroot /data/gentoo /bin/bash -c "source /etc/profile"

EOF