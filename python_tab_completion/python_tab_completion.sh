#!/bin/bash
# Setup Tab Completion for Python Interactive Shell
# http://stackoverflow.com/questions/246725/how-do-i-add-tab-completion-to-the-python-shell

cat <<EOT >> ~/.pythonrc
# ~/.pythonrc
# enable syntax completion
try:
    import readline
except ImportError:
    print("Module readline not available.")
else:
    import rlcompleter
    readline.parse_and_bind("tab: complete")
EOT

echo "export PYTHONSTARTUP=~/.pythonrc" >> ~/.bashrc
