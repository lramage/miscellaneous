from gi.repository import Gtk
import random
import math

class dice(Gtk.Window):

    def __init__(self):
       Gtk.Window.__init__(self, title="Dice")
       self.set_border_width(10)
       self.set_default_size(400, 200)

       hbox = Gtk.Box(spacing=6)
       self.add(hbox)

       button = Gtk.Button.new_with_label("Roll")
       button.connect("clicked", self.on_roll_clicked)
       hbox.pack_start(button, True, True, 0)	

    def on_roll_clicked(self, button):
       button.set_label(str(random.randint(1,6)))

win = dice()
win.connect("delete-event", Gtk.main_quit)
win.show_all()
Gtk.main()
