#!/bin/bash
# Setup environment for building ubuntu touch

USER=`whoami`

# Get phablet tools
sudo add-apt-repository ppa:phablet-team/tools
sudo apt-get update
sudo apt-get install phablet-tools

# Get build tools
sudo apt-get install git gnupg flex bison gperf build-essential \
  zip bzr curl libc6-dev libncurses5-dev:i386 x11proto-core-dev \
  libx11-dev:i386 libreadline6-dev:i386 libgl1-mesa-glx:i386 \
  libgl1-mesa-dev g++-multilib mingw32 tofrodos \
  python-markdown libxml2-utils xsltproc zlib1g-dev:i386 schedtool \
  g++-4.8-multilib
 
cd /srv
sudo mkdir phablet
sudo chown $USER:$USER phablet
phablet-dev-bootstrap phablet
cd phablet/

export USE_CCACHE=1

if [ -d build ]; then
  . build/envsetup.sh
fi

echo "Setup completed"