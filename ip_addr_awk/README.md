# Get IP Addresses With Awk

    ip addr show dev eth0 | awk 'FNR==3 { print $4 }'
