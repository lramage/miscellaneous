#!/bin/bash
# Get ip addresses using awk

# All interfaces
ip addr | awk '/inet\s/ { split($2, ip, "/"); print ip[1] }'

# Specified interface
ip addr show dev eth0 | awk '/inet\s/ { split($2, ip, "/"); print ip[1] }'