# GPD XD Plus: can't enable WiFi

`Resolved: factory reset`

Purchased on Amazon here: https://www.amazon.com/GPD-Portable-Handheld-Touchscreen-Quad-Core/dp/B01B4ESXT0

See also:

- [[Recovery] TWRP 3.2.1-0 for GPD XD Plus / XD+ Handheld MT8176](https://forum.xda-developers.com/android/development/recovery-twrp-3-2-1-0-gpd-xd-plus-xd-t3754134)

- [GPD Devices | Dingoonity.org - The Dingoo Community](https://boards.dingoonity.org/gpd-android-devices)

- [libretro/RetroArch: Input problems with GPD XD device #2420](https://github.com/libretro/RetroArch/issues/2420)