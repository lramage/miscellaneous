# Manual Override
Instantly Switch Default Routes

> "A manual override (MO) or manual analog override (MAO) is a mechanism wherein control is taken from an automated system and given to the user." - [Wikipedia](https://en.wikipedia.org/wiki/Manual_override)

![](https://imgs.xkcd.com/comics/manual_override.png)
