#!/bin/bash
# Alpine Linux on Raspberry Pi 2 Emulated via Qemu
set -euo pipefail
IFS=$'\n\t'

HOMEPAGE="https://alpinelinux.org"
PKG="alpine-rpi-3.6.2-armhf"
SRC_URI="http://dl-cdn.alpinelinux.org/alpine/v3.6/releases/armhf/${PKG}.tar.gz"

# Replace s/./_/g for image name
IMG="$( echo ${PKG} | awk '{ gsub(/\./, "_"); print }' ).img"
IMG_SIZE="1G"

WORKDIR="/mnt/${PKG}"

RAM="1024"

# fetch source
wget "${SRC_URI}"

# create image
qemu-img create -f raw "${IMG}" "${IMG_SIZE}"

# partition image
parted -s -a optimal "${IMG}" \
                  mklabel gpt \
                  mkpart primary 0% 100%

# Sector size of (512) x Start of partition (2048)
OFFSET="1048576"

# setup loopback device
losetup -o "${OFFSET}" -f "${IMG}"
LOOP_DEV=$(losetup -j "${IMG}" | grep -o "/dev/loop[0-9]*")

# format loop device
mkfs.ext4 "${LOOP_DEV}"

# mount rootfs
mkdir "${WORKDIR}" && \
mount -t ext4 "${LOOP_DEV}" "${WORKDIR}"

# extract rootfs
tar -xzvf "${PKG}.tar.gz" -C "${WORKDIR}"

# Unmount rootfs
umount "${WORKDIR}"

# Detach loop device
losetup -d "${LOOP_DEV}"

# Create qemu launcher
cat << EOF > rpi2-qemu.sh
#!/bin/bash
# Run rpi2 alpine image in qemu

qemu-system-arm \
-machine raspi2 \
-m ${RAM} \
-hda ${IMG}

EOF