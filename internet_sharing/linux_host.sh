#!/bin/bash
# linux host configuration

ip link set up dev eth0
ip addr add 10.10.10.101/24 dev eth0

sysctl net.ipv4.ip_forward=1

iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i eth0 -o wlan0 -j ACCEPT