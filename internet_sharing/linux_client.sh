#!/bin/bash
# linux client configuration

ip addr add 10.10.10.201/24 dev eth0
ip link set up dev eth0
ip route add default via 10.10.10.101 dev eth0