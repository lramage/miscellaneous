#!/bin/bash
# Generate Hostname

# Helper Functions
check-dependency() { 
  which "$@" &> /dev/null
  echo "$?"
}

get-system-info() {
  export _SYSINFO=`dmidecode | grep -A3 '^System Information'`
  echo "$?"
}

# Main
if [ ! check-dependency dmidecode ]; then
#awk '{ split($0, a, "."); print a[2] }'`
get-system-info
echo $_SYSINFO
fi