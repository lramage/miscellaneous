#!/bin/bash
# Copyright 2017 Lucas Ramage
# Distributed under the terms of the GNU General Public License v2

# The purpose of this scipt is to setup a simple cronjob 
# that will report any ip address changes of a server via email.

echo "Enter an email address to be notified, followed by [ENTER]:"

read EMAIL_ADDR

cat << EOF > /tmp/check_ip.sh
#!/bin/bash
# Check to see if primary ip address has changed

function send_notification() {
sendmail "${EMAIL_ADDR}" <<EOM
subject: IP Address Changed

Last known ip address: "${CURRENT_ADDR}"
EOM

}

CURRENT_ADDR=`ip route | awk '/default/ { $3 }'`

if [ -f /tmp/ ip_addr ]; then
  OLD_ADDR=`cat /tmp/ip_addr`
  
  if [ "${CURRENT_ADDR}" == "${OLD_ADDR}" ]; then
    # ip address has not changed
    return 0
  else
    # ip address has changed
    send_notification
  fi
else
   echo "${CURRENT_ADDR}" > /tmp/ip_addr
   send_notification
fi

EOF

chmod +x /tmp/check_ip.sh

(crontab -l ; echo "*/30 * * * * /tmp/check_ip.sh")| crontab -