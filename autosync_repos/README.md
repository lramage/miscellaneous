# Automatically sync git remote repositories with github

I would like to automatically sync github, bitbucket,
and gitlab so that I have redundancy in place for all of my repositories.

https://api.github.com/users/lramage94/repos

![](https://upload.wikimedia.org/wikipedia/commons/f/f1/Henri_Maillardet_automaton%2C_London%2C_England%2C_c._1810_-_Franklin_Institute_-_DSC06656.jpg)

See also,

- https://robots.thoughtbot.com/keeping-a-github-fork-updated
