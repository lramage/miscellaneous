#!/bin/bash
# Batch import ebuilds

# usage: P_TREE=~/repos/gentoo P_OVERLAY=~/repos/portage-overlay ./batch_import_ebuilds.sh ebuilds.txt

while read MY_P;
do
  echo "Importing ${MY_P}.."
  
  # Package category
  MY_PC=`echo "${MY_P}" | awk '{ split($0,a,/\//); print a[1] }'`
  
  cp -R "${P_TREE}/${MY_P}" "${P_OVERLAY}${MY_PC}"
done < $1